var jcrop_api;
localStorage.getItem("current_pallet") == "null" ? localStorage.setItem("current_pallet", "2") ?? localStorage.getItem("current_pallet") : false;
jQuery(document).ready(function() {
//initJcrop();
    //function initJcrop(){
    //    jcrop_api = jQuery.Jcrop('#cropbox1');
    //}
    jQuery.noConflict();

    var cropped_url = jQuery("#cropped_url").val();
    jQuery("#cropped_image").val(cropped_url);
// 	(function() {
    //var canvas = new fabric.Canvas('canvas');
    //localStorage.setItem("attachment_url", null);
    var current_pallet = localStorage.getItem("current_pallet");
    if (current_pallet === "null") {
        localStorage.setItem("current_pallet",current_pallet);
    }
	var bg_default_image = "https://duschrueckwand-online.de/wp-content/gallery/bergsee/meer1.jpg";

    //Set Default image
    var clicked_image = localStorage.getItem("choosenimg");
    jQuery("#lb_clicked_image").val(clicked_image);

    //Image Gal Tabs
    jQuery(".tab1content").show();
    jQuery(".tab2content").hide();
    jQuery(".tab3content").hide();
    jQuery(".tab4content").hide();
    jQuery(".tab5content").hide();
    jQuery(".tab6content").hide();
    jQuery(".tab7content").hide();
    jQuery(".tab8content").hide();
    jQuery(".tab9content").hide();
    jQuery(".tab10content").hide();
    jQuery(".tab11content").hide();
    jQuery(".tab12content").hide();
    jQuery(".tab13content").hide();
    jQuery(".tab14content").hide();
    jQuery(".tab15content").hide();
    jQuery(".tab16content").hide();
    
    jQuery("#tabsselection .tab1").addClass("active");
    jQuery( "#tabsselection .tabssel" ).click(function(e) {
        
        //Remove active class
        jQuery("#tabsselection .tabssel").each(function(index){
            jQuery(this).removeClass("active");
        });
        var selectedtab = jQuery(this).attr("data-val");
        jQuery(this).addClass("active");
        //////console.log("selectedtab.."+selectedtab);
        jQuery(".galcontent").each(function(index){
            var contenttab = jQuery(this).attr("data-val");
            //////console.log("contenttab.."+contenttab);
            if (contenttab == selectedtab) {
                jQuery(this).show();            
            }else{
                jQuery(this).hide();
            }
        });
    });
    
    //Default material selection Price
    var total_price_default = jQuery("#total_price_default").val();
    //total_price = parseFloat(total_price_default);
   // jQuery("#total_price").val(total_price);
    //jQuery(".product-price-container .price .amount:last").html(total_price);
    //jQuery("h3 span#total_price").html(total_price);

    //Set Selected Size Price on selection
    jQuery("#selected_mat_price").val(15);
    jQuery("#selected_material").val("Alu Struktur Optik");

    jQuery('#detailSelect .matbox').on('click', function() {    
        var total_price_default = jQuery("#total_price_default").val();
        var selected_mat_price = jQuery(this).attr("data-price");
        var selected_mat_value = jQuery(this).attr("data-title");

        //Selected material name
        var selected_mat_price = jQuery(this).attr("data-price");
        jQuery("#selected_material").val(selected_mat_value);
        
        var range_price = jQuery("#range_price").val();

        jQuery("#detailSelect .matbox").each(function(index){
            jQuery(this).removeClass("matactive");
        });
        jQuery(this).addClass("matactive");   
        jQuery("#selected_mat_price").val(selected_mat_price);
        setPrice();
    });

    jQuery('.servicesamogus').on('click', function(){
        //Selected material name
        var range_price = jQuery("#range_price").val();
        var service = jQuery(this).attr("data-service");
        jQuery("#selected_service_price").val(service);
        jQuery(".servicesamogus").each(function(index){
            jQuery(this).removeClass("matactive");
        });
        jQuery('#selected_service').val(service);
        selected_service_price = (service == "grafikservice") ? 30 : 0;
        jQuery('#selected_service_price').val(selected_service_price);
        console.log(selected_service_price);
        jQuery(this).addClass("matactive");
        setPrice();
    })
    
    //Get Sqcm
    jQuery("#width0,#width1,#width2,.platecount").on("change click", function(e) {
        setPrice();
    });

    //Change Platte
    // jQuery( "#save_size" ).click(function(e) {
    //     localStorage.setItem("croppedimg", localStorage.getItem('croppedimg') ?? localStorage.getItem('choosenimg'));
    // });
    jQuery("#reset").click(function(e) {
        localStorage.removeItem('croppedimg');
        jQuery("#palletarea").css("background-image", "url(" + localStorage.getItem("choosenimg") + ")");
        (localStorage.getItem("current_pallet") ?? 0) == "1" ? jQuery('#cropbox1').Jcrop({setSelect: [500,0,7000, 8000]}) : false;
        (localStorage.getItem("current_pallet") ?? 0) == "1" ? jQuery("#lb_crop_submit").click() : false;
    });
    //Default current pallet to 2
    var current_pallet = localStorage.getItem("current_pallet");
    localStorage.setItem("current_pallet", current_pallet);
    
    jQuery( "#platec1,#platec2,#platec3" ).click(function(e) {
        
        jQuery("#palletarea").css({'background-image': 'url('+(clicked_image)+')'});
        changePlates(jQuery(this).attr("data-val"));
        localStorage.setItem("current_pallet",jQuery(this).attr("data-val"));
        updateView();
        // reloadPage();
        
    });
    localStorage.setItem("pageReload","1");
    function reloadPage(){
       var i = localStorage.getItem("pageReload"); 
       if (i == 1) {
        location.reload();
       } 
       localStorage.setItem("pageReload","2"); 
    }
        //jQuery( "#imagegalleriesparent .categoryicons .galcontent .ngg-gallery-thumbnail a" ).click(function(e) {
        //jQuery( ".ngg-gallery-thumbnail-box .ngg-gallery-thumbnail a" ).click(function(e) {
        //alert(jQuery(this).attr('data-val'));
        
        //jQuery( ".ngg-gallery-thumbnail-box .ngg-gallery-thumbnail a" ).each(function(index) {
        //jQuery(this).on("click", function(){
        jQuery(document).on("click", '.ngg-gallery-thumbnail-box .ngg-gallery-thumbnail a', function(e) { 
        //document.querySelector(".ngg-gallery-thumbnail-box .ngg-gallery-thumbnail a").addEventListener('click', e => {        

            setTimeout(function(){ 
                jQuery("html").removeClass("has-lightbox");
                jQuery("body").removeClass("hidden-scroll");
            }, 900);
            

            e = e || window.event;
            e.preventDefault();
            e.stopPropagation();
            
            var clicked_image  = jQuery(this).attr('href');
            bg_default_image = clicked_image;
            //////console.log("clicked_image.."+clicked_image);
            localStorage.setItem("choosenimg",clicked_image);
            jQuery(".product-template-default.single.single-product").css("padding-right","0px");
            updateView();
            checkDims();
            //Change image of cropper
            //jcrop_api.setImage(clicked_image); 
            //jQuery("#cropbox1").attr("src",clicked_image);
            //location.reload();
            var currentURL = jQuery(location).attr("href");
            window.location.href= currentURL;
    });

    function setPrice(){
        //Get Price form Backend
        var range_1_from = parseInt(document.getElementById('range_1_from').value);
        var range_1_to = parseInt(document.getElementById('range_1_to').value);
        var range_1_price = parseInt(document.getElementById('range_1_price').value);
        var range_2_from = parseInt(document.getElementById('range_2_from').value);
        var range_2_to = parseInt(document.getElementById('range_2_to').value);
        var range_2_price = parseInt(document.getElementById('range_2_price').value);
        var range_3_from = parseInt(document.getElementById('range_3_from').value);
        var range_3_to = parseInt(document.getElementById('range_3_to').value);
        var range_3_price = parseInt(document.getElementById('range_3_price').value);
        var range_4_from = parseInt(document.getElementById('range_4_from').value);
        var range_4_to = parseInt(document.getElementById('range_4_to').value);
        var range_4_price = parseInt(document.getElementById('range_4_price').value);
        var price_above_range_4 = parseInt(document.getElementById('price_above_range_4').value);
        var range_price;

        //Reset total prices
        //jQuery("#total_price").val(0.00);
        //jQuery("#range_price").val(0.00);
        //jQuery("#total_price_default").val(0.00);
        var userwidth0 = jQuery("#width0").val();
        var userwidth1 = jQuery("#width1").val();
        var userwidth2 = jQuery("#width2").val();
        var height = jQuery("#pHeight").val();

        var current_pallet = localStorage.getItem("current_pallet");
        if (current_pallet == 1) {
            var squaremm = parseInt(userwidth0) * parseInt(height);
        }if (current_pallet == 2) {
            var squaremm = (parseFloat(userwidth0) + parseFloat(userwidth1)) * parseInt(height);
        }if (current_pallet == 3) {
            var squaremm = (parseFloat(userwidth0) + parseFloat(userwidth1) + parseFloat(userwidth2)) * parseInt(height);
            console.log("squaremm..3."+squaremm);
        }

        console.log(`******* Ranges 1 ********`);
        console.log(`range_1_from:${range_1_from}`);
        console.log(`range_1_to:${range_1_to}`);
        console.log(`squaremm:${squaremm}`);
        console.log(`******* Ranges ********`);

        console.log(`******* Ranges 2 ********`);
        console.log(`range_2_from:${range_2_from}`);
        console.log(`range_2_to:${range_2_to}`);
        console.log(`squaremm:${squaremm}`);
        console.log(`******* Ranges ********`);

        console.log(`******* Ranges 4 ********`);
        console.log(`range_4_from:${range_4_from}`);
        console.log(`range_4_to:${range_4_to}`);
        console.log(`squaremm:${squaremm}`);
        console.log(`******* Ranges ********`);

        //Applying price according to range
        if (squaremm > range_1_from && squaremm <= range_1_to){
            //alert(`Range 1..${range_1_price}`);
            range_price = range_1_price;
           // jQuery("#range_price").val(range_price);
        }
        if (squaremm >= range_2_from && squaremm <= range_2_to){
            //alert(`Range 2..${range_2_price}`);
            range_price = range_2_price;
            //jQuery("#range_price").val(range_price);
        }
        if (squaremm >= range_3_from && squaremm <= range_3_to){
            //alert(`Inside Range 3..${range_2_price}`);
            range_price = range_3_price;
            //jQuery("#range_price").val(range_price);
        }
        if (squaremm >= range_4_from && squaremm <= range_4_to){
            //alert(`Range 2..${range_2_price}`);
            range_price = range_4_price;
            //jQuery("#range_price").val(range_price);
        }
        if (squaremm >= range_4_to){
            //alert(`Range 2..${range_2_price}`);
            range_price = price_above_range_4;
            //jQuery("#range_price").val(range_price);
        }
        // if (parseFloat(squaremm) >= parseFloat(range_3_from) && parseFloat(squaremm) <= parseFloat(range_3_to)){
        //     //alert(range_3_price);
        //     range_price = parseFloat(range_3_price);
        //     jQuery("#range_price").val(range_price);
        // }
        // if (parseFloat(squaremm) >= parseFloat(range_4_from) && parseFloat(squaremm) <= parseFloat(range_4_to)){
        //     //alert(range_4_price);
        //     range_price = parseFloat(range_4_price);
        //     jQuery("#range_price").val(range_price);
        // }
        // if (parseFloat(squaremm) >= parseFloat(range_4_to) ){
        //     range_price = parseFloat(price_above_range_4);
        //     jQuery("#range_price").val(range_price);
        // }
        console.log(`range_price:${range_price}`);
        //get selected material price
        var selected_mat_price = jQuery("#selected_mat_price").val();
        
        //get selected material price
        var selected_service_price = jQuery("#selected_service_price").val();

        //Get current product price
        var current_prod_price = jQuery("#total_price_default").val();
        //var range_price = jQuery("#range_price").val();;

        console.log(`***********`);
        console.log("selected_service_price.."+selected_service_price);
        console.log("current_prod_price.."+current_prod_price);
        console.log(`range_price..${range_price}`);
        console.log(`selected_mat_price..${selected_mat_price}`);
        console.log(`***********`);

        var total_price = parseFloat(selected_service_price) + parseFloat(current_prod_price) + parseFloat(range_price) + parseFloat(selected_mat_price);
        
         console.log("range_1_price.."+range_1_price);      
         console.log("range_2_price.."+range_2_price);      
         console.log("range_3_price.."+range_3_price);      
         console.log("range_4_price.."+range_4_price);      
         console.log("squaremm.."+squaremm);      
         console.log("selected_mat_price.."+selected_mat_price);      
         console.log("current_prod_price.."+current_prod_price);
         console.log("range_price.."+range_price);
         console.log("total_price.."+total_price);

        //Set price
        jQuery("#total_price").val(total_price.toFixed(2));
        jQuery("#total_price_second").html(total_price.toFixed(2));
        jQuery(".product-price-container .price .amount:last").html(total_price);
        jQuery("h3 span#total_price").html(total_price);
    }

	// canvas.setBackgroundImage(bg_image, canvas.renderAll.bind(canvas),{
	// 	top: parseInt(side_a_bg_top),
	// 	left: parseInt(side_a_bg_left), 
	// 	scaleX: parseInt(side_a_bg_scale),
	// 	scaleY: 1		
	// });
// })();
// });
/*
    maker.js for plateART.de

    Heavily intertwined with the PHP part of the app.
    Handles cutting, selecting and moving of the image
    on different materials.

    CAUTION: heavy mix of synchronous, asynchronous and global variable use
    (c) by rs(at)metadist.de - October 2020
*/
// ------------------------------------------------------- GLOBAL VARIABLES
// ---------------- define graphical values

var motherSnippet = [0,0,100,100];
var defaultSizesMinMax = [];
defaultSizesMinMax["standard"] = [100,100,1500,3000];
defaultSizesMinMax["kitchen"] = [100,100,3000,1500];
defaultSizesMinMax["quattro"] = [100,100,1000,3000];

var defaultPixelHeight = [];
defaultPixelHeight["standard"] = 580;
defaultPixelHeight["kitchen"] = 560;
defaultPixelHeight["quattro"] = 500;

var defaultType = "standard";  // standard 4500x3000 - kitchen 3000x1500 - quattro 3000x3000
var plateCount = 2;

var minMaxMM = [];
minMaxMM["standard"] = [100,100,1500,3000];
minMaxMM["kitchen"] = [100,100,3000,1500];
minMaxMM["quattro"] = [100,100,1000,3000];

var defPlateCount = [];
defPlateCount["standard"] = 2;
defPlateCount["kitchen"] = 1;
defPlateCount["quattro"] = 2;

var defSizes = [];
defSizes["standard"] = [900,2000];
defSizes["kitchen"] = [2000,600];
defSizes["quattro"] = [900,2000];

var plateStartDefault = [0,0];
var totalPlatesWidth = defaultSizesMinMax[defaultType][2]*3;
var totalPlatesHeight = defaultSizesMinMax[defaultType][3];

var pshare = [];

var flipMother = 0;
var zoomMother = 1;
//
var fullWidth = minMaxMM[defaultType][2]*3;
var fullHeight = minMaxMM[defaultType][3];

var showWidth = 100;
var showHeight = 100;

var motherRatio = fullWidth / fullHeight;
var selectionRatio = fullWidth / fullHeight;

// ----------------------------------------------------------------- snipper settings
var startPercentX = 0;
var startPercentY = 0;
var endPercentX = 100;
var endPercentY = 100;

var displayWidth = 640;
var displayHeight = 400;

// selection box
var snipX = 0, snipY = 0, snipW = 0, snipH = 0;
var startZoom = 0;

// selectionRation smaller than motherRation? then Full Height used!
// else full Width used!
var relSnipStartXPercent = 0;
var relSnipStartYPercent = 0;


jQuery("#width0,#width1,#width2,#pHeight").on("change", function(e) {
    checkDims();
})
// jQuery("#pimg0,#pimg1,#pimg2").on("change", function(e) {
//     equalizeHeight();
// })

// ------------------------------------------------------------------------------------------------
// main functions
// ------------------------- calc start values
function calcStartValues() {
    getTotalPlatesWidth();

    zoomMother = 1;
    fullWidth = defaultSizesMinMax[defaultType][2]*3;
    fullHeight = defaultSizesMinMax[defaultType][3];

    if(defaultType=="quattro") {
        motherRatio = 1;
    } else if(defaultType=="standard") {
        motherRatio = 1.5;
    } else {
        motherRatio = 4;
    }
    selectionRatio = totalPlatesWidth / totalPlatesHeight;

    if (selectionRatio < motherRatio) {
        // full height
        showWidth = parseFloat((100 / motherRatio * selectionRatio)).toFixed(4);
        relSnipStartXPercent = 50 - parseFloat((showWidth / 2)).toFixed(4);
    } else if (selectionRatio > motherRatio) {
        // full width
        showHeight = parseFloat((100 / selectionRatio * motherRatio)).toFixed(4);
        relSnipStartYPercent = 50 - parseFloat((showHeight / 2)).toFixed(4);
    } else {
        showWidth = 100;
        showHeight = 100;
        relSnipStartXPercent = 0;
        relSnipStartYPercent = 0;
    }
    plateStartDefault = [relSnipStartXPercent,relSnipStartYPercent];
}
function initSnipper() {
    //////////console.log("Init: " + flipMother);
    jQuery("#cropBox").load(defaultImage+"maker/snipper.php?id="+defaultImage.toString()+"&flip="+flipMother.toString()+"&touring=" + Math.random().toString(10));
}

function checkDims() {
    var totalHeight = jQuery("#pHeight").val();
    // set height
    // previous
    /*
    if(1==2) {
        if (totalHeight < minMaxMM[defaultType][1]) {
            alert("Mindesthöhe: " + minMaxMM[defaultType][1].toString());
            jQuery("#pHeight").val(minMaxMM[defaultType][1]);
        }
        if (totalHeight > minMaxMM[defaultType][3]) {
            alert("Maximalhöhe: " + minMaxMM[defaultType][3].toString());
            jQuery("#pHeight").val(minMaxMM[defaultType][3]);
        }
        // ---------------------
        var plate = 0;
        var pw = [];
        pw[0] = jQuery("#width0").val();
        pw[1] = jQuery("#width1").val();
        pw[2] = jQuery("#width2").val();

        for (plate = 0; plate < plateCount; plate++) {
            if (pw[plate] < minMaxMM[defaultType][0]) {
                alert("Mindestbreite: " + minMaxMM[defaultType][0].toString());
                jQuery("#width" + plate.toString()).val(minMaxMM[defaultType][0]);
            }
            if (pw[plate] > minMaxMM[defaultType][2]) {
                alert("Maximalbreite: " + minMaxMM[defaultType][2].toString());
                jQuery("#width" + plate.toString()).val(minMaxMM[defaultType][2]);
            }
        }
    }
    */
    // fix checks if 4500x3000 is busted in any direction
    var dimHintText = "Unsere Platten können auf von 100 x 100 bis zu 3.000 x 1.500 Millimeter geschnitten werden.\nWenn Ihre Höhe 1.500 überschreitet, liegt die maximale Breite bei 1.500 und entsprechend für die Breite ebenso.";
    var plate = 0;
    var pw = [];
    var platesTotalWidthSum = 0;
    var showProfiHint = true;

    pw[0] = jQuery("#width0").val();
    pw[1] = jQuery("#width1").val();
    pw[2] = jQuery("#width2").val();

    if(totalHeight > 3000) {
        alert(dimHintText);
        jQuery("#pHeight").val(3000);
        totalHeight = 3000;
    }
    if(totalHeight < 100) {
        alert(dimHintText);
        jQuery("#pHeight").val(100);
        totalHeight = 100;
    }

    for (plate = 0; plate < plateCount; plate++) {
        if (pw[plate] < minMaxMM[defaultType][0]) {
            alert("Mindestbreite: " + minMaxMM[defaultType][0].toString());
            jQuery("#width" + plate.toString()).val(minMaxMM[defaultType][0]);
        }
        if(totalHeight > 1500) {
            if (pw[plate] > 1500) {
                alert("Maximalbreite: 1.500 mm!\n\n" + dimHintText);
                jQuery("#width" + plate.toString()).val(1500);
                showProfiHint = false;
            }
        } else {
            if (pw[plate] > 3000) {
                alert("Maximalbreite: 3.000 mm!\n\n" + dimHintText);
                jQuery("#width" + plate.toString()).val(3000);
            }
        }
        platesTotalWidthSum = platesTotalWidthSum + parseInt(pw[plate],10);
    }

    if((platesTotalWidthSum > 4500) && showProfiHint==true) {
        alert("Bei einer Gesamtbreite von über 4.500 mm (4,5m) müssen wir zu Qualitätskontrolle des Bildes (Auflösung) den Profi-Schnitt aktivieren.");
        setService('ProfiSnippet');
    }
    // MINIMAL = 200x300
    if((totalHeight*platesTotalWidthSum < 60000)) {
        alert("Die Gesamtfläche unterschreitet 60 cm² !");
        jQuery("#pHeight").val(200);
        jQuery("#width0").val(300);
    }

    // update view and set values
    zoomMother = 1;
    totalPlatesWidth = getTotalPlatesWidth();
    changePlates(plateCount);
    a = (localStorage.getItem("current_pallet") ?? 0) == "1" ? jQuery('#cropbox1').Jcrop({setSelect: [0,0,7000, 8000]}) : false;
    a =(localStorage.getItem("current_pallet") ?? 0) == "1" ? jQuery("#lb_crop_submit").click() : false;
    a = (localStorage.getItem("current_pallet") ?? 0) == "2" ? jQuery('#cropbox1').Jcrop({setSelect: [0,0,7000, 8000]}) : false;
    a = (localStorage.getItem("current_pallet") ?? 0) == "2" ? jQuery("#lb_crop_submit").click() : false;
    a = (localStorage.getItem("current_pallet") ?? 0) == "3" ? jQuery('#cropbox1').Jcrop({setSelect: [0,0,7000, 8000]}) : false;
    a = (localStorage.getItem("current_pallet") ?? 0) == "3" ? jQuery("#lb_crop_submit").click() : false;
}
checkDims();
// ------------------------------------------------------------------------------------------------
function getTotalPlatesWidth() {
    var plate = 0;
    totalPlatesWidth = 0;
    totalPlatesHeight = parseInt(jQuery("#pHeight").val(),10);
    var pw = [];
    pw[0] = parseInt(jQuery("#width0").val(),10);
    pw[1] = parseInt(jQuery("#width1").val(),10);
    pw[2] = parseInt(jQuery("#width2").val(),10);
    for(plate=0; plate < plateCount; plate++) {
        totalPlatesWidth = totalPlatesWidth + pw[plate];
    }
    return totalPlatesWidth;
}
// ------------------------------------------------------------------------------------------------
function updatePrice() {
    // var materialId = jQuery("#materialId").val();
    // var totalHeight = parseInt(jQuery("#pHeight").val(),10);
    // var totalQm = 0;
    // var snipService = jQuery("input[name='snippetService']:checked").val();

    // // sum up the plates that are used
    // var pw = [];

    // pw[0] = jQuery("#width0").val();
    // jQuery("#width0cm").html(pw[0]/10 + " cm");

    // pw[1] = jQuery("#width1").val();
    // jQuery("#width1cm").html(pw[1]/10 + " cm");

    // pw[2] = jQuery("#width2").val();
    // jQuery("#width2cm").html(pw[2]/10 + " cm");

    // // take width calculations per plate centered from image
    // totalPlatesWidth = getTotalPlatesWidth();

    // totalQm = totalHeight * totalPlatesWidth / (1000*1000);

    // jQuery.getJSON(urlRel+"maker/app.php?action=getPrice&matId=" + materialId.toString() + "&qm=" + totalQm.toString() + "&serv=" + snipService.toString(), function(data) {
    //    jQuery("#priceValue").html(new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(data.price));
    // });

    // updateMusterPrice();
}
// ------------------------------------------------------------------------------------------------
function updateMusterPrice() {
    // var materialId = jQuery("#materialId").val();
    // var totalHeight = 200;
    // var totalQm = 0;
    // var snipService = "SelfSnippet";

    // // sum up the plates that are used
    // var pw = [];

    // // take width calculations per plate centered from image
    // // totalPlatesWidth = getTotalPlatesWidth();

    // totalQm = 200 * 300 / (1000*1000);

    // jQuery.getJSON(urlRel+"maker/app.php?action=getPrice&matId=" + materialId.toString() + "&qm=" + totalQm.toString() + "&serv=" + snipService.toString(), function(data) {
    //     jQuery("#musterprice").html(new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(data.price));
    // });
}

// ------------------------------------------------------------------------------------------------
function toogleMatSelectors(arrKeys) {
    var loopCount = 0;
    var matId = 0;
    var VisibleIds = "";
    var HiddenIds = "";
    var latestVisibleMatId = 0;

    jQuery("#detailSelect").children("div").each(function(index) {
        activeDivText = jQuery(this).text();
        activeDivId = jQuery(this).attr("id");
        matId = parseInt(activeDivId.replace("mat",""));

        jQuery("#" + activeDivId).show();

        ////////console.log("Active DIV id: " + activeDivId + " -- ");
        ////////console.log("Active text: " + activeDivText + " in DIV");

        if(arrKeys.length > 0) {
           var hideDiv = true;
           arrKeys.forEach(function(allowedText) {
               allowedText = allowedText.replace("Material: ","");
               ////////console.log("Allowed is: " + allowedText);
               if(activeDivText.search(allowedText) > -1) {
                   hideDiv = false;
                   ////////console.log("Searching " + allowedText + " in " + activeDivText + " - MatID: " + latestVisibleMatId);
                   if(latestVisibleMatId==0) {
                       latestVisibleMatId = matId;
                   }
               }
           });

           if(hideDiv) {
               jQuery("#" + activeDivId).hide();
               HiddenIds = HiddenIds + "|"+matId+"|";
           }
        }
        loopCount++;
    });

    if(HiddenIds.search("|"+defaultMat+"|") > -1 && latestVisibleMatId>0) {
        matSelect(latestVisibleMatId);
    }
}

// ------------------------------------------------------------------------------------------------
function setDefaultImage(id) {
    // defaultImage = id;
    // myThumbId = id;
    // ////////console.log(id);
    // // ------------------------------------------------------------------
    // // set default type to quattro, if tag is set:
    // //jQuery.getJSON(urlRel+"http://localhost/stepform/wp-content/uploads/2021/05/app.jpeg" + defaultImage.toString(), function(data) {
    // jQuery.getJSON("http://localhost/stepform/wp-content/uploads/2021/05/app.jpeg", function(data) {
    //     defaultType = data["orientation"];
    //     matLimit = data["materialLimit"];
    //     toogleMatSelectors(matLimit);
    //     //alert(defaultType);
    //     motherSnippet = [0,0,100,100];
    //     plateStartDefault = [0,0];
    //     zoomMother = 1;
    //     flipMother = 0;
    //     selectionRatio = fullWidth / fullHeight;

    //     // ----------------------------------------------------------------- snipper settings
    //     startPercentX = 0;
    //     startPercentY = 0;
    //     endPercentX = 100;
    //     endPercentY = 100;

    //     zoomMother = 1;
    //     fullWidth = minMaxMM[defaultType][2]*3;
    //     fullHeight = minMaxMM[defaultType][3];

    //     motherRatio = fullWidth / fullHeight;

    //     showWidth = 100;
    //     showHeight = 100;

    //     //selection box
    //     snipX = 0, snipY = 0, snipW = 0, snipH = 0;

    //     startZoom = 0;
    //     // selectionRation smaller than motherRation? then Full Height used!
    //     // else full Width used!
    //     relSnipStartXPercent = 0;
    //     relSnipStartYPercent = 0;

    //     jQuery("#platec3").prop("checked", true);
    //     jQuery("#imageName").html(data["filename"]);
    //     changePlates(plateCount);
    // });
}
// ------------------------------------------------------------------------------------------------
function matSelect(myId) {
    //jQuery("#detailSelect .matbox").removeClass("matactive");
    //jQuery("#mat"+myId.toString()).addClass("matactive");
   // jQuery("#materialId").val(myId);
   // defaultMat = myId;
    //////console.log("defaultMat..."+defaultMat);
    //////////console.log("Material: " + jQuery("#materialId").val());
    updatePrice();
}
// ------------------------------------------------------------------------------------------------
function catSelect(myId) {
    myCatId = myId;
    motherSnippet = [0,0,100,100];

    //console.log("Coming from: "+defaultType);

    jQuery("#catNav").children().removeClass("mactive");
    jQuery("#cat" + myId.toString()).addClass("mactive");

    zoomMother = 1;
    flipMother = 0;

    //jQuery("#imageList").load(urlRel+"maker/app.php?action=getImageList&id=" + myId.toString() + "&myThumbId=" + (0 + myThumbId).toString());
    jQuery("#imageList").load(bg_default_image);
}
    function setMMDefaults() {
        // //////console.log("Height: "+ defaultType + ": " + defSizes[defaultType][1]);
        jQuery("#width0").val(defSizes[defaultType][0]);
        jQuery("#width1").val(defSizes[defaultType][0]);
        jQuery("#width2").val(defSizes[defaultType][0]);
        jQuery("#pHeight").val(defSizes[defaultType][1]);

        totalPlatesWidth = getTotalPlatesWidth();

        //reset ratio
        //motherRatio = (defaultSizesMinMax[defaultType][2] * 3) / defaultSizesMinMax[defaultType][3];
        if(defaultType=="quattro") {
            motherRatio = 1;
        } else if(defaultType=="standard") {
            motherRatio = 1.5;
        } else {
            motherRatio = 4;
        }


        selectionRatio = totalPlatesWidth / totalPlatesHeight;
        ////////console.log("**** DefaultRatio:"+motherRatio);
        changePlates(plateCount);
    }  
    function changePlates(myCount) {

        plateCount = myCount;
        zoomMother = 1;

        var gh = parseInt(jQuery("#pHeight").val(),10);
        totalPlatesWidth = getTotalPlatesWidth();

        jQuery("#platec1").removeClass("plateactive");
        jQuery("#platec2").removeClass("plateactive");
        jQuery("#platec3").removeClass("plateactive");

        jQuery("#platec" + plateCount.toString()).addClass("plateactive");

        // percentage width of spans to allow bigger images for 1 and 2 plates
        var maxW0 = parseInt(jQuery("#width0").val() / totalPlatesWidth * 99.5 ,10);
        var maxW1 = parseInt(jQuery("#width1").val() / totalPlatesWidth * 99.5 ,10);
        var maxW2 = parseInt(jQuery("#width2").val() / totalPlatesWidth * 99.5 ,10);

        // get ratio and set start X and start Y
        // show / hide and change classes by amount of plates
        if(parseInt(plateCount, 10) === 3) {

            jQuery("#vcol0").removeClass("verticalImage2");
            jQuery("#vcol0").removeClass("verticalImage1");
            jQuery("#vcol0").addClass("verticalImage3");
            jQuery("#vcol0").css("max-width", maxW0+"%");
            jQuery("#vcol0").css("text-align","right");

            jQuery("#vcol1").removeClass("verticalImage2");
            jQuery("#vcol1").removeClass("verticalImage1");
            jQuery("#vcol1").addClass("verticalImage3");
            jQuery("#vcol1").css("max-width", maxW1+"%");
            jQuery("#vcol1").css("text-align","center");

            jQuery("#vcol2").removeClass("verticalImage2");
            jQuery("#vcol2").removeClass("verticalImage1");
            jQuery("#vcol2").addClass("verticalImage3");
            jQuery("#vcol2").css("max-width", maxW2+"%");
            jQuery("#vcol2").css("text-align","left");

            jQuery("#vcol2").show();
            jQuery("#vcol1").show();

            // winputs
            jQuery("#wInput0").removeClass("verticalImage2");
            jQuery("#wInput0").removeClass("verticalImage1");
            jQuery("#wInput0").addClass("verticalImage3");
            jQuery("#wInput0").css("width","30%");

            jQuery("#wInput1").removeClass("verticalImage2");
            jQuery("#wInput1").removeClass("verticalImage1");
            jQuery("#wInput1").addClass("verticalImage3");
            jQuery("#wInput1").css("width","30%");

            jQuery("#wInput2").removeClass("verticalImage2");
            jQuery("#wInput2").removeClass("verticalImage1");
            jQuery("#wInput2").addClass("verticalImage3");
            jQuery("#wInput2").css("width","30%");

            jQuery("#wInput1").show();
            jQuery("#wInput2").show();

            plateStartDefault = [0, plateStartDefault[1]];
        }
        if(parseInt(plateCount, 10) === 2) {
            jQuery("#vcol0").removeClass("verticalImage3");
            jQuery("#vcol0").removeClass("verticalImage1");
            jQuery("#vcol0").addClass("verticalImage2");
            jQuery("#vcol0").css("max-width", maxW0+"%");
            jQuery("#vcol0").css("text-align","right");

            jQuery("#vcol1").removeClass("verticalImage3");
            jQuery("#vcol1").removeClass("verticalImage1");
            jQuery("#vcol1").addClass("verticalImage2");
            jQuery("#vcol1").css("max-width", maxW1+"%");
            jQuery("#vcol1").css("text-align","left");

            jQuery("#vcol2").hide();
            jQuery("#vcol1").show();

            // winputs
            jQuery("#wInput0").removeClass("verticalImage3");
            jQuery("#wInput0").removeClass("verticalImage1");
            jQuery("#wInput0").addClass("verticalImage2");
            jQuery("#wInput0").css("width","50%");

            jQuery("#wInput1").removeClass("verticalImage3");
            jQuery("#wInput1").removeClass("verticalImage1");
            jQuery("#wInput1").addClass("verticalImage2");
            jQuery("#wInput1").css("width","50%");

            jQuery("#wInput2").hide();
            jQuery("#wInput1").show();

            plateStartDefault = [16, plateStartDefault[1]];
        }
        if(parseInt(plateCount, 10) === 1) {
            jQuery("#vcol0").removeClass("verticalImage2");
            jQuery("#vcol0").removeClass("verticalImage3");
            jQuery("#vcol0").addClass("verticalImage1");
            jQuery("#vcol0").css("max-width", maxW0+"%");
            jQuery("#vcol0").css("text-align","center");
            jQuery("#wInput0").css("width","99%");

            jQuery("#vcol2").hide();
            jQuery("#vcol1").hide();

            jQuery("#wInput2").hide();
            jQuery("#wInput1").hide();
            plateStartDefault = [33, plateStartDefault[1]];
        }

        // selectionRation smaller than motherRatio? then Full Height used!
        // else full Width used!
        var relSnipStartXPercent = 0;
        var relSnipStartYPercent = 0;

        selectionRatio = totalPlatesWidth / gh;

        if(selectionRatio < motherRatio) {
            // full height
            showWidth =  parseFloat((100 / motherRatio * selectionRatio)).toFixed(4);
            relSnipStartXPercent = 50 - parseFloat((showWidth / 2)).toFixed(4);
        } else if(selectionRatio > motherRatio) {
            // full width
            showHeight =  parseFloat((100 / selectionRatio * motherRatio)).toFixed(4);
            relSnipStartYPercent = 50 - parseFloat((showHeight / 2)).toFixed(4);
        }
        plateStartDefault = [relSnipStartXPercent,relSnipStartYPercent];
        ////console.log(plateStartDefault);
        updateView();
        setPrice();
    }
    jQuery("#nmber_of_pallets").val(2);
    
    //Get plate Counts
    jQuery("div.platecount").click(function() {
        jQuery(this).attr('data-val') == 1 ? jQuery('#cropbox1').Jcrop({setSelect: [500,0,7000, 8000]}) : false;
        jQuery(this).attr('data-val') == 1 ? jQuery("#lb_crop_submit").click() : false;
        var current_pallet = jQuery(this).attr('data-val');
        jQuery("#nmber_of_pallets").val(current_pallet);
        localStorage.setItem("current_pallet", current_pallet);
        localStorage.removeItem("croppedimg");
        changePlates(current_pallet);
        setPrice();
        var cropped_image = localStorage.getItem("croppedimg");
        //On Pallet selection remove cropped image and switch to full image
        var clicked_image = localStorage.getItem("choosenimg");
        //console.log("clicked_image..Pat..change"+clicked_image);
        if (clicked_image === undefined || clicked_image === null) {
            jQuery("#palletarea").css("background-image", "url('https://duschrueckwand-online.de/wp-content/gallery/bergsee/meer1.jpg')");
            //localStorage.setItem("choosenimg","https://duschrueckwand-online.de/wp-content/gallery/bergsee/meer1.jpg"); 
        }
        ////console.log("clicked_image..Pat..change"+clicked_image);
        jQuery("#palletarea").css("background-image", "url(" + (clicked_image) + ")");
    });
// jQuery(".img-container").load(function() {
//     updateView();
// });    
    function updateView(){
        var plate;
        var p , pdomId;
        var gh;
        var pw = [];
        var total_width_calc_three_set;
        var total_width_calc_set;

        gh = parseInt(jQuery("#pHeight").val(),10);
        jQuery("#pHeightcm").html(gh/10 + " cm");

        pw[0] = jQuery("#width0").val();
        jQuery("#width0cm").html(pw[0]/10 + " cm");

        pw[1] = jQuery("#width1").val();
        jQuery("#width1cm").html(pw[1]/10 + " cm");

        pw[2] = jQuery("#width2").val();
        jQuery("#width2cm").html(pw[2]/10 + " cm");

        // take width calculations per plate centered from image
        totalPlatesWidth = getTotalPlatesWidth();
        console.log(" Falling in Update view ");
        
        // calc part of each plate
        // height is calculated in PHP from gh
        for(plate=0; plate < plateCount; plate++) {
            pshare[plate] = parseFloat((pw[plate] / totalPlatesWidth * 100)).toFixed(4);
            //////console.log(pw[0]);
            //////console.log(pw[1]);
            //////console.log(pw[2]);
            //////console.log("pshare[0].."+pshare[0]);
            //////console.log("pshare[1].."+pshare[1]);
            //////console.log("pshare[2].."+pshare[2]);
            jQuery("#pallet0").css("margin-left",pshare[0]+'%');
            jQuery("#pallet1").css("margin-left",pshare[1]+'%');
            jQuery("#pallet2").css("margin-left",pshare[2]+'%');

            //Plate divider on cropper
            localStorage.setItem("divider1left",pshare[0]);
            localStorage.setItem("divider2left",pshare[1]);

            var first_pallet_left = parseFloat(pshare[0]) / 100 * parseFloat(pw[0]);
            var second_pallet_left = parseFloat(pshare[1]) / 100 * parseFloat(pw[1]);
            //////console.log("pshare[2].."+pshare[2]);
            //////console.log("pshare[2].."+pw[2]);
            var third_pallet_left = parseFloat(pshare[2]) / 100 * parseFloat(pw[2]);
            //////console.log("third_pallet_left",third_pallet_left);
            var second_pallet_total = parseFloat(first_pallet_left) + parseFloat(second_pallet_left);
            //var third_pallet_total = parseFloat(first_pallet_left) + parseFloat(second_pallet_left) + parseFloat(third_pallet_left);
            //////console.log("third_pallet_total.."+third_pallet_total);

            // if (first_pallet_left >= 450){
            //     first_pallet_left = 400;
            // }if (second_pallet_left >= 450){
            //     second_pallet_left = 400;
            // }if (third_pallet_left >= 450){
            //     third_pallet_left = 400;
            // }

            //36% calculation of gh height
            var calcheightpx = parseFloat(30.625) / 100 * parseFloat(gh);  
            console.log("calcheightpx.."+calcheightpx)
            if (calcheightpx >= parseInt(700)) {
                calcheightpx = 700;
            }
            var current_pallet = localStorage.getItem("current_pallet");
            ////console.log("current_pallet...Called..."+current_pallet);
            jQuery("#no-of-pallets").val(current_pallet);
            
            //Get width and height of cropper area and snipper
            displayWidth = parseInt(jQuery("#cropbox1").width(), 10);
            displayHeight = parseInt(jQuery("#cropbox1").height(), 10);
            
            // WIDTH
            snipW = parseInt(displayWidth / 100 * (0.9), 10);           

            // HEIGHT
            snipH = parseInt(displayHeight / 100 * (0.9), 10);

            if (current_pallet == 1){
                jQuery("#pallet0,#pallet1,#pallet2").hide();
                //document.getElementById("divider2").style.display = 'none';
                jQuery("#divider2.jcrop-border.ord-w").css("display", "none");
                localStorage.setItem("divider2left",0);
                var total_width = parseFloat(pw[0]);
                var total_width_calc = parseFloat(total_width);
                //////console.log("total_width_calc..1.."+total_width_calc);
                document.getElementById("palletarea").style.width = (total_width_calc/2);
                jQuery("#palletarea").css("width",(total_width_calc/3));
                jQuery("#palletarea").width(total_width_calc/3);
                jQuery("#lb_w").val(total_width_calc);
                jQuery("#lb_h").val(calcheightpx);
                jQuery("#palletarea").css("height", calcheightpx);
                //////console.log("Falling in Pallet 1");
                           
                //Set cropper size
                var width = jQuery(window).width();
                    jQuery('#cropbox1').Jcrop({ 
                            onChange: updateCoords,
                            onSelect: updateCoords,
                            // minSize:[3,3],
                            // maxSize:[snipW,snipH],
                            setSelect: [0,0,7000, 8000],
                            aspectRatio:  total_width_calc / gh,
                            allowResize: false,
                            allowSelect: false,
                            boxWidth: jQuery(document).width() > 778 ? 778 : jQuery(document).width() - 10,
                            // boxHeight: jQuery(document).height() > 778 ? 519 : jQuery(document).height() - 10                  
                    });
                //}

            }
            if(current_pallet == 2){
                jQuery("#pallet1,#pallet2").hide();
                jQuery("#pallet0").show();
                var total_width = parseFloat(pw[0]) + parseFloat(pw[1]);
                var total_width_calc = parseFloat(total_width) / parseFloat(2);
                //console.log("total_width_calc..2.."+total_width_calc);
                // Set max width to 800
                if (parseFloat(total_width_calc) > parseFloat(800.00)) {
                    total_width_calc_set = 800;
                }
                jQuery("#palletarea").css("width",total_width_calc);
                jQuery("#lb_w").val(total_width_calc);
                jQuery("#lb_h").val(calcheightpx);
                jQuery("#palletarea").width(total_width_calc_set);
                jQuery("#palletarea").css("height", calcheightpx);
                document.getElementById("palletarea").style.width = total_width_calc;

                jQuery('#cropbox1').Jcrop({ 
                        onChange: updateCoords,
                        onSelect: updateCoords,
                        // minSize:[3,3],
                        // maxSize:[snipW,snipH],
                        setSelect: [0,0,7000, 8000],
                        aspectRatio: total_width / gh,
                        allowResize: false,
                        allowSelect: false,
                        boxWidth: jQuery(document).width() > 778 ? 778 : jQuery(document).width() - 10,
                        // boxHeight: jQuery(document).height() > 778 ? 519 : jQuery(document).height() - 10
                });
                //}
            }
            if(current_pallet == 3){            
                //canvas.clear();
                jQuery("#pallet0,#pallet1,#pallet2").show();
                ////console.log("Falling in Pallet 3");                      
                var total_width_three = parseFloat(pw[0]) + parseFloat(pw[1]) + parseFloat(pw[2]);
                var total_width_calc_three = parseFloat(total_width_three) / parseFloat(3);

                // Set max width to 800
                if (parseFloat(total_width_calc_three) > parseFloat(800.00)) {
                    total_width_calc_three_set = 800;
                }

                console.log("total_width_calc..3.."+total_width_calc_three); 

                jQuery("#palletarea").css("width",total_width_calc_three);
                jQuery("#lb_w").val(total_width_calc_three);
                jQuery("#lb_h").val(calcheightpx);
                jQuery("#palletarea").css("height", gh);
                jQuery("#palletarea").width(total_width_calc_three_set);
                document.getElementById("palletarea").style.width = total_width_calc_three;

                //Set cropper size
                var jcrop_api, boundx, boundy;
                jQuery('#cropbox1').Jcrop({ 
                        onChange: updateCoords,
                        onSelect: updateCoords,
                        // minSize:[3,3],
                        // maxSize:[snipW,snipH],
                        setSelect: [0,0,7000, 8000],
                        aspectRatio: total_width_three / gh,
                        allowResize: false,
                        allowSelect: false,
                        boxWidth: jQuery(document).width() > 778 ? 778 : jQuery(document).width() - 10 ,
                        // boxHeight: jQuery(document).height() > 519 ? 519 : jQuery(document).height() - 10                   
                }); 
            }

            p = plate;
            pdomId = p.toString();
        }

        //Plates container width
        var plates_cont_width = parseFloat(totalPlatesWidth) / parseFloat(3); 
        //jQuery("#mainplateswrap").css({"max-width": plates_cont_width});
        var cropped_image = localStorage.getItem("croppedimg");
        var clicked_image = localStorage.getItem("choosenimg");
        jQuery("#lb_clicked_image").val(clicked_image);
        //////console.log("clicked_image.."+clicked_image);
        if (!clicked_image){
            clicked_image = "https://duschrueckwand-online.de/wp-content/gallery/bergsee/meer1.jpg";
            jQuery("#cropbox1").attr("src","https://duschrueckwand-online.de/wp-content/gallery/bergsee/meer1.jpg");
            localStorage.setItem("choosenimg","https://duschrueckwand-online.de/wp-content/gallery/bergsee/meer1.jpg"); 
        }

        //Set image on Crop area
        jQuery("#cropbox1").attr("src",clicked_image);
        //jQuery("#preview").attr("src",clicked_image);

        //Static Div Bg
        var attachment_url = localStorage.getItem("choosenimg");
     
        if ( jQuery("#cropped_url").length ){
            jQuery("#palletarea").css("background-image", "url(" + attachment_url + ")");
        }else{
            jQuery("#palletarea").css("background-image", "url(" + (clicked_image) + ")");
        }
        jQuery("#palletarea").css("height", calcheightpx);
        jQuery("#selected_image").val(clicked_image);

    }
        //Set Normal Bg Size
        jQuery("#palletarea").css("background-size",`${jQuery(pHeight)}`);
        // jQuery("#palletarea").css("background-repeat","no-repeat");

        //Click on Crop submit
        jQuery("#lb_crop_submit" ).click(function() {
            var cropped_url = jQuery("#cropped_url").val();
            jQuery("#cropped_image").val(cropped_url);
            jQuery("#palletarea").css("background-size","contain");
            var lb_clicked_image = jQuery("#lb_clicked_image").val();
            var lb_x = jQuery("#lb_x").val();
            var lb_y = jQuery("#lb_y").val();
            var lb_cropperw = jQuery("#lb_w").val();
            var lb_h = jQuery("#lb_h").val();
            var flipit = jQuery("#flipit").val();
            
            jQuery.post(
                my_ajax_object.ajax_url, 
                {
                    'mirror': flipit,
                    'action': 'foobar',
                    'lb_clicked_image': lb_clicked_image,
                    'lb_x':   lb_x,
                    'lb_y':   lb_y,
                    'lb_cropperw':   lb_cropperw,
                    'lb_h':   lb_h
                }, 
                function(response) {
                    //var data = JSON.parse(xmlhttp.responseText);
                    //document.getElementById("myDiv").innerHTML = data.attachment_url;
                    ////console.log('The server responded: ',response);
                    localStorage.setItem("croppedimg", response);
                    jQuery("#palletarea").css({'background-image': 'url('+response+')'});
                    var modal = document.getElementById("myModal");
                    modal.style.display = "none";
                }
            );
        }); 
        //Click on Crop
        // jQuery("#crop_image" ).click(function() {
        //     //////console.log("chal ja bhai");
        //     //Call ajax
        //     //var universal_leadid = jQuery("#leadid_token").val();
        //     var clicked_image = localStorage.getItem("choosenimg");
        //     var x = jQuery("#x").val();
        //     var y = jQuery("#y").val();
        //     var w = jQuery("#w").val();
        //     var h = jQuery("#h").val();
            
        //     jQuery.post(
        //         my_ajax_object.ajax_url, 
        //         {
        //             'action': 'foobar',
        //             'clicked_image': clicked_image,
        //             'x': x,
        //             'y': y,
        //             'w': w,
        //             'h': h
        //         }, 
        //         function(response) {
        //             ////console.log('The server responded: ', response);
        //             jQuery("#preview").attr("src","data:image/png;base64,"+response);
        //         }
        //     );
        // });

// ------------------------------------------------------------------------------------------------
function cleanHeight() {
    var pdomId = "";
    var plate;

    for (plate = 0; plate < plateCount; plate++) {
        pdomId = "#pimg" + plate.toString();
        jQuery(pdomId).css({"height": "auto"});
        //////////console.log("Auto: Plate " + pdomId );
    }
}
// ------------------------------------------------------------------------------------------------
function equalizeHeight() {
    var p;
    var equalHeight = 0;
    var setAtEnd = defaultPixelHeight[defaultType];
    var pdomId = "";
    var plate;

    for (plate = 0; plate < plateCount; plate++) {
        pdomId = "#pimg" + plate.toString();
        jQuery(pdomId).css({"height": "auto"});
        //////////console.log("Auto: Plate " + pdomId );
    }

    if(plateCount > 1) {
        // --------------- now check
        for (plate = 0; plate < plateCount; plate++) {
            p = plate;
            pdomId = "#pimg" + p.toString();
            equalHeight = jQuery(pdomId).height();
            //////////console.log("Plate " + pdomId +": " + equalHeight);
            if (equalHeight > 20) {
                if (equalHeight < setAtEnd) {
                    setAtEnd = equalHeight;
                }
            }
        }

        if(setAtEnd > 1) {
            for (plate = 0; plate < plateCount; plate++) {
                p = plate;
                pdomId = "#pimg" + p.toString();
                //////////console.log("Plate " + plate + " Height:" +setAtEnd.toString())
                jQuery(pdomId).css({"height": setAtEnd.toString() + "px"});
            }
        }
    }
}
var defaultImage = 8389;
var defaultMat = 1;
var myThumbId = 0;
var myCatId = 0;
var urlRel = '/';
// Get the modal
var modal = document.getElementById("myModal");
// Get the button that opens the modal
var btn = document.getElementById("myBtn");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
jQuery(document).ready(function() {
//loadStorage();
//jQuery( window ).resize(function() {
//updateView();
//});
});
function cutService() {
	modal.style.display = "inline-block";
	modal.style.position = "fixed";
	modal.style.zIndex = "99";
	jQuery(".section-content").css({"z-index":"auto"});
	initSnipper();
	return false;
}
function setService(myVal) {
	jQuery("input[name='snippetService']:checked").val(myVal);
	if(myVal=="ProfiSnippet") {
	jQuery("#ProfiSnippet").addClass("matactive");
	jQuery("#SelfSnippet").removeClass("matactive");
	} else {
	jQuery("#SelfSnippet").addClass("matactive");
	jQuery("#ProfiSnippet").removeClass("matactive");
	}
}
function scrollCatRight() {
	//jQuery("#matLeftArrow").show();
	var x = jQuery("#catHeader").scrollLeft();
	var newX = x + 100;
	jQuery("#catHeader").scrollLeft(newX);
	if(x == jQuery("#catHeader").scrollLeft()) {
	//jQuery("#matRightArrow").hide();
	}
}
function scrollCatLeft() {
	// jQuery("#matRightArrow").show();
	var x = jQuery("#catHeader").scrollLeft();
	var newX = x - 100;
	if(newX < 0) {
	newX = 0;
	}
	jQuery("#catHeader").scrollLeft(newX);
	if(newX == 0) {
	// jQuery("#matLeftArrow").hide();
	}
}
function scrollImgRight() {
	//jQuery("#matLeftArrow").show();
	var x = jQuery("#imagesHeader").scrollLeft();
	var newX = x + 120;
	jQuery("#imagesHeader").scrollLeft(newX);
	if(x == jQuery("#imagesHeader").scrollLeft()) {
	//jQuery("#matRightArrow").hide();
	}
}
function scrollImgLeft() {
	// jQuery("#matRightArrow").show();
	var x = jQuery("#imagesHeader").scrollLeft();
	var newX = x - 120;
	if(newX < 0) {
	newX = 0;
	}
	jQuery("#imagesHeader").scrollLeft(newX);
	if(newX == 0) {
	// jQuery("#matLeftArrow").hide();
	}
}
//
// function setStorage() {
// 	if (typeof (Storage) !== "undefined") {
// 	localStorage.setItem("lastCatId", myCatId);
// 	localStorage.setItem("lastImageId", defaultImage);
// 	localStorage.setItem("lastMatId", defaultMat);
// 	localStorage.setItem("lastHeight", totalPlatesHeight);
// 	localStorage.setItem("lastWidth", totalPlatesWidth);
// 	localStorage.setItem("lastW0", parseInt(jQuery("#width0").val(),10));
// 	localStorage.setItem("lastW1", parseInt(jQuery("#width1").val(),10));
// 	localStorage.setItem("lastW2", parseInt(jQuery("#width2").val(),10));
// 	localStorage.setItem("lastPlates", plateCount);
// 	}
// }
// function loadStorage() {
// 	if (typeof (Storage) !== "undefined") {
// 	if( 0 + parseInt(localStorage.getItem("lastCatId"),10) > 0 ) {
// 	// set last values
// 	jQuery("#width0").val(parseInt(localStorage.getItem("lastW0"),10));
// 	jQuery("#width1").val(parseInt(localStorage.getItem("lastW1"),10));
// 	jQuery("#width2").val(parseInt(localStorage.getItem("lastW2"),10));
// 	jQuery("#pHeight").val(parseInt(localStorage.getItem("lastHeight"),10));
// 	plateCount = parseInt(localStorage.getItem("lastPlates"),10)
// 	myThumbId = 0 + parseInt(localStorage.getItem("lastImageId"),10);
// 	//jQuery.getJSON(urlRel+"maker/app.php?action=getImageInfo&imageId=" + myThumbId.toString(), function(data) {
// 	// defaultType = data["orientation"];
// 	// jQuery("#imageName").html(data["filename"]);
// 	// calcStartValues();
// 	// catSelect(parseInt(localStorage.getItem("lastCatId"),10));
// 	// ////////console.log("Storage values found");
// 	// });
// 	} else {
// 	myCatId = 41;
// 	////////console.log("Cat ID found");
// 	calcStartValues();
// 	changePlates(2);
// 	}
// 	//jQuery.getJSON(urlRel+"maker/app.php?action=getImageInfo&imageId=" + defaultImage.toString(), function(data) {
// 	// jQuery("#imageName").html(data["filename"]);
// 	// });
// 	} else {
// 	myCatId = 41;
// 	calcStartValues();
// 	changePlates(2);
// 	}
// }
function clearSettings() {
	if (typeof (Storage) !== "undefined") {
	localStorage.clear();
	location.reload();
	}
}
   window.jssor_1_slider_init = function() {

    var jssor_1_options = {
      $AutoPlay: 0,
      $AutoPlaySteps: 6,
      $SlideDuration: 160,
      $SlideWidth: 180,
      $SlideHeight: 65,
      $SlideSpacing: 6,
      $ArrowNavigatorOptions: {
        $Class: $JssorArrowNavigator$,
        $Steps: 6
      },
      $BulletNavigatorOptions: {
        $Class: $JssorBulletNavigator$,
        $SpacingX: 16,
        $SpacingY: 16
      }
    };

    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

    /*#region responsive code begin*/

    var MAX_WIDTH = 1280;
    var MAX_Height = 120;

    function ScaleSlider() {
        var containerElement = jssor_1_slider.$Elmt.parentNode;
        var containerWidth = containerElement.clientWidth;

        if (containerWidth) {

            var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

            jssor_1_slider.$ScaleWidth(expectedWidth);
        }
        else {
            window.setTimeout(ScaleSlider, 10);
        }
    }

    ScaleSlider();
    $Jssor$.$AddEvent(window, "load", ScaleSlider);
    $Jssor$.$AddEvent(window, "resize", ScaleSlider);
    $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
    /*#endregion responsive code end*/
};
jssor_1_slider_init();

//Jquerycrop
//  window.addEventListener('DOMContentLoaded', function () {
//   var image = document.querySelector('#image');
//   var result = document.querySelector('#result');
//   var cropper = new Cropper(image, {
//     ready: function () {
//       var image = new Image();

//       image.src = cropper.getCroppedCanvas().toDataURL('image/jpeg');
//       result.appendChild(image);
//     },
//   });
// });
    
    //Jcrop
     //jQuery(function(){
        // jQuery('#cropbox1').Jcrop({ 
        //     aspectRatio: 1,
        //     minSize:[366.25,514.28],
        //     maxSize:[366.25,514.28],
        //     setSelect: [ 300, 0, 300, 300 ],
        //     allowSelect: false,
        //     allowMove: true,
        //     allowResize: true,
        //     onChange: updateCoords,
        //     onSelect: updateCoords,
        //     boxWidth: 780,
        //     boxHeight: 600 
        // });


    //     // for sample 2
    //     var api = jQuery.Jcrop('#cropbox2',{ // we linking Jcrop to our image with id=cropbox1
    //         setSelect: [ 100, 100, 200, 200 ]
    //     });
    //     var i, ac;
    //     // A handler to kill the action
    //     function nothing(e) {
    //         e.stopPropagation();
    //         e.preventDefault();
    //         return false;
    //     };
    //     // Returns event handler for animation callback
    //     function anim_handler(ac) {
    //         return function(e) {
    //             api.animateTo(ac);
    //             return nothing(e);
    //         };
    //     };
    //     // Setup sample coordinates for animation
    //     var ac = {
    //         anim1: [0,0,40,600],
    //         anim2: [115,100,210,215],
    //         anim3: [80,10,760,585],
    //         anim4: [105,215,665,575],
    //         anim5: [495,150,570,235]
    //     };
    //     // Attach respective event handlers
    //     for(i in ac) jQuery('#'+i).click(anim_handler(ac[i]));
    //     // for sample 3
        // jQuery('#cropbox3').Jcrop({ // we linking Jcrop to our image with id=cropbox3
        //     setSelect: [ 20, 130, 480, 230 ],
        //     addClass: 'jcrop_custom',
        //     bgColor: 'blue',
        //     bgOpacity: .5,
        //     sideHandles: false,
        //     minSize: [ 50, 50 ],
        //     boxWidth: 780,
        //     boxHeight: 550, 
        // });
    // });
    jQuery("#flipit").on("change",function(){
        if (jQuery("#flipit").val() == "true"){
            jQuery(".jcrop-active").css({
                '-webkit-transform' : 'scaleX(-1)',
                '-moz-transform'    : 'scaleX(-1)',
                '-ms-transform'     : 'scaleX(-1)',
                '-o-transform'      : 'scaleX(-1)',
                'transform' : 'scaleX(-1)'
            });
        }
        else{
            jQuery(".jcrop-active, #palletarea").css({
                '-webkit-transform' : 'unset',
                '-moz-transform'    : 'unset',
                '-ms-transform'     : 'unset',
                '-o-transform'      : 'unset',
                'transform' : 'unset'
            });
        }
    });
    jQuery("#mirror").click(function(){
        if (jQuery("#flipit").val() != "true"){
            jQuery("#mirror").addClass("matactive");
            jQuery("#flipit").val("true");
            jQuery("#flipit").trigger("change");
        }
        else{
            jQuery("#mirror").removeClass("matactive");
            jQuery("#flipit").val("false");
            jQuery("#flipit").trigger("change");
        }
    });
    function updateCoords(c) {
        jQuery('#lb_x').val(c.x);
        jQuery('#lb_y').val(c.y);
        jQuery('#lb_w').val(c.w);
        jQuery('#lb_h').val(c.h);
        jQuery('#lb_x2').val(c.x2);
        jQuery('#lb_y2').val(c.y2);
        var rx = 200 / c.w; // 200 - preview box size
        var ry = 200 / c.h;
        // jQuery('#preview').css({
        //     width: Math.round(rx * 800) + 'px',
        //     height: Math.round(ry * 600) + 'px',
        //     marginLeft: '-' + Math.round(rx * c.x) + 'px',
        //     marginTop: '-' + Math.round(ry * c.y) + 'px'
        // });
        jQuery("#divider1").remove();
        jQuery("#divider2").remove();
        jQuery(".jcrop-selection.jcrop-current").append("<div id='divider1' class='jcrop-border ord-w'> <div>");
        jQuery(".jcrop-selection.jcrop-current").append("<div id='divider2' class='jcrop-border ord-w'> <div>");
        var divider1left = localStorage.getItem("divider1left");
        var divider2left = localStorage.getItem("divider2left");
        var second_cut = parseFloat(divider1left) + parseFloat(divider2left);
        //Cut 1
        var element = document.getElementById("divider1");
        if(typeof(element) != 'undefined' && element != null){
            jQuery("#divider1").css("left",divider1left+'%'); 
        }
        //Cut 2
        var element1 = document.getElementById("divider2");
        if(typeof(element1) != 'undefined' && element1 != null){        
            jQuery("#divider2").css("left",second_cut+'%'); 
        }
    };
    // jQuery(window).load(function(){
    //     //jQuery("#accordion").accordion({autoHeight: false,navigation: true});
    // });
    // function checkCoords() {
    //     if (parseInt(jQuery('#w').val())) return true;
    //     alert('Please select a crop region then press submit.');
    //     return false;
    // };

    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the button that opens the modal
    var cropper_model = document.getElementById("cropper_model");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];   

    //Set Widths and Height data on reload
     var pHeight = localStorage.getItem("pHeight") ?? localStorage.setItem("pHeight", 1000) ?? localStorage.getItem("pHeight");
     var width0 = localStorage.getItem("width0") ?? localStorage.setItem("width0", 1500) ?? localStorage.getItem("width0");
     var width1 = localStorage.getItem("width1") ?? localStorage.setItem("width1", 1500) ?? localStorage.getItem("width1");
     var width2 = localStorage.getItem("width2") ?? localStorage.setItem("width2", 1500) ?? localStorage.getItem("width2");

     if (pHeight) {
        jQuery("#pHeight").val(pHeight);
     }
     if (width0) {
        jQuery("#width0").val(width0);
     }
     if (width1) {
        jQuery("#width1").val(width1);
     }
     if (width2) {
        jQuery("#width2").val(width2);
     }
    // When the user clicks on the button, open the modal
    cropper_model.onclick = function() {

        modal.style.display = "grid";
        jQuery(".fusion-header-wrapper.fusion-is-sticky").css("z-index","1");

        //adding croper on popup
        // jQuery('#cropbox3').Jcrop({ // we linking Jcrop to our image with id=cropbox3
        //     setSelect: [ 20, 130, 480, 230 ],
        //     addClass: 'jcrop_custom',
        //     bgColor: 'blue',
        //     bgOpacity: .5,
        //     sideHandles: false,
        //     minSize: [ 50, 50 ],
        //     boxWidth: 780,
        //     boxHeight: 550, 
        // });

        //get width and height data
        var pHeight = jQuery("#pHeight").val();
        var width0 = jQuery("#width0").val();
        var width1 = jQuery("#width1").val();
        var width2 = jQuery("#width2").val();
        localStorage.setItem("pHeight",pHeight);
        localStorage.setItem("width0",width0);
        localStorage.setItem("width1",width1);
        localStorage.setItem("width2",width2);
        jQuery(".jcrop-active canvas").trigger("click");
        //document.getElementById("myCheck").click();
        var current_pallet = localStorage.getItem("current_pallet") ?? localStorage.setItem("current_pallet", "2") ?? localStorage.getItem("current_pallet");
        ////console.log("current_pallet.."+current_pallet);
        if (current_pallet == 1) {
            jQuery("#divider2").css("display", "none");
            jQuery("#divider2").css("height", "0px !important");
            jQuery("#divider2").css("width", "0px !important");
        }
        //changePlates(current_pallet);
        //updateView();
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
      modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
    //Show cropped if user cropped an image
    if ( jQuery("#cropped_url").length ){
        var attachment_url = jQuery("#cropped_url").val();
        ////console.log("Cropped.."+attachment_url)
        localStorage.setItem("attachment_url", attachment_url);
        jQuery("#palletarea").css("background-image", "url(" + attachment_url + ")"); 
    }else{
        jQuery("#palletarea").css("background-image", "url('https://duschrueckwand-online.de/wp-content/gallery/bergsee/meer1.jpg')"); 
    }
    
    //Open current selected pallets Default Page load  
    var current_pallet = localStorage.getItem("current_pallet");
    console.log("current_pallet.load."+current_pallet);
    //current_pallet = 2;
    //changePlates(current_pallet);
    //updateView();
    // jQuery("#platec"+current_pallet).trigger("click");

    if (current_pallet === "null") {
        //console.log("current_pallet....inner"+current_pallet);
        current_pallet = 2;
        changePlates(current_pallet);
        updateView();
        jQuery("#platec2").trigger("click");
    }else{
        changePlates(current_pallet);
        updateView();
    }
    
        //set values on local Storage
        jQuery("#width0,#width1,#width2,#pHeight").on("change click", function(e) {
            //get width and height data
            var pHeight = jQuery("#pHeight").val();
            var width0 = jQuery("#width0").val();
            var width1 = jQuery("#width1").val();
            var width2 = jQuery("#width2").val();
            localStorage.setItem("pHeight",pHeight);
            localStorage.setItem("width0",width0);
            localStorage.setItem("width1",width1);
            localStorage.setItem("width2",width2); 
        });

        //Set default values on page reload
        var pHeight = localStorage.getItem("pHeight");
        var width0 = localStorage.getItem("width0");
        var width1 = localStorage.getItem("width1");
        var width2 = localStorage.getItem("width2");

        jQuery("#pHeight").val(pHeight);
        jQuery("#width0").val(width0);
        jQuery("#width1").val(width1);
        jQuery("#width2").val(width2);

    jQuery("#ordermuster").click(function(){
        jQuery('#platec1').click();
        jQuery('#pHeight').val(300);
        jQuery('#width0').val(200);
        jQuery('#total_price').val("4.9");
        localStorage.removeItem('current_pallet');
        localStorage.removeItem('width0');
        localStorage.removeItem('width1');
        localStorage.removeItem('width2');
        localStorage.removeItem('pHeight');
        setTimeout(function(){jQuery(".add_to_cart_btn").click();}, 2000);
    });

});
